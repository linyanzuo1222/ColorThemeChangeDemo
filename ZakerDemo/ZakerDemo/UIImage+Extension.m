//
//  UIImage+MJ.m
//  04-图片裁剪
//
//  Created by apple on 14-4-14.
//  Copyright (c) 2014年 itcast. All rights reserved.
//

#import "UIImage+Extension.h"
#import <objc/message.h>

@implementation UIImage (NJ)

+ (instancetype)circleImageWithName:(NSString *)name Size:(CGSize)size {
    UIImage *oldImage = [UIImage imageNamed:name];
    
    // 开启上下文
    CGSize imageSize = oldImage.size;
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0);
    
    // 取得当前上下文
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // 画圆, 裁剪
    CGFloat radius = size.width > size.height ? size.height / 2 : size.width / 2;
    CGContextAddArc(context, imageSize.width / 2, imageSize.height / 2, radius, 0, M_PI * 2, 0);
    CGContextClip(context);
    
    // 画图
    [oldImage drawInRect:CGRectMake(0, 0, oldImage.size.width, oldImage.size.height)];
    
    // 取图
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // 关闭上下文
    UIGraphicsEndImageContext();
    
    return newImage;
}

+ (instancetype)circleImageWithName:(NSString *)name borderWidth:(CGFloat)borderWidth borderColor:(UIColor *)borderColor
{
    // 1.加载原图
    UIImage *oldImage = [UIImage imageNamed:name];
    
    // 2.开启上下文
    CGFloat imageW = oldImage.size.width + 2 * borderWidth;
    CGFloat imageH = oldImage.size.height + 2 * borderWidth;
    CGSize imageSize = CGSizeMake(imageW, imageH);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0);
    
    // 3.取得当前的上下文
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    // 4.画边框(大圆)
    [borderColor set];
    CGFloat bigRadius = imageW * 0.5; // 大圆半径
    CGFloat centerX = bigRadius; // 圆心
    CGFloat centerY = bigRadius;
    CGContextAddArc(ctx, centerX, centerY, bigRadius, 0, M_PI * 2, 0);
    CGContextFillPath(ctx); // 画圆
    
    // 5.小圆
    CGFloat smallRadius = bigRadius - borderWidth;
    CGContextAddArc(ctx, centerX, centerY, smallRadius, 0, M_PI * 2, 0);
    // 裁剪(后面画的东西才会受裁剪的影响)
    CGContextClip(ctx);
    
    // 6.画图
    [oldImage drawInRect:CGRectMake(borderWidth, borderWidth, oldImage.size.width, oldImage.size.height)];
    
    // 7.取图
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // 8.结束上下文
    UIGraphicsEndImageContext();
    
    return newImage;
}\

- (instancetype)thumbnailImageWithSize:(CGSize)targetSize {
    CGSize sourceSize = self.size;
    
    // Size一样时直接返回原图
    if (CGSizeEqualToSize(sourceSize, targetSize)) return self;
    
    CGFloat widthfFactor = targetSize.width / sourceSize.width;
    CGFloat heightFactor = targetSize.height / sourceSize.height;
    // scaleFactor 等于较小的比例.
    CGFloat scaleFactor = widthfFactor < heightFactor ? heightFactor : widthfFactor;
    
    // 获取放大/缩小后保持比例的宽高
    CGFloat scaleWidth = sourceSize.width * scaleFactor;
    CGFloat scaleHeight = sourceSize.height * scaleFactor;
    
    CGPoint thumbnailPoint = CGPointZero;
    if (widthfFactor > heightFactor) {
        thumbnailPoint.x = (targetSize.width - scaleWidth) * 0.5;
    } else if (widthfFactor < heightFactor) {
        thumbnailPoint.y = (targetSize.height - scaleHeight) * 0.5;
    }
    
    UIGraphicsBeginImageContext(targetSize);
    CGRect thumbnailRect = CGRectMake(0, 0, scaleWidth, scaleHeight);
    thumbnailRect.origin = thumbnailPoint;
    [self drawInRect:thumbnailRect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    if ( !newImage) {
        NSLog(@"创建图片失败");
    }
    
    return newImage;
}

@end



















