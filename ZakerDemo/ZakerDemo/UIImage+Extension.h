//
//  UIImage+MJ.h
//  04-图片裁剪
//
//  Created by apple on 14-4-14.
//  Copyright (c) 2014年 itcast. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Extension)

/** 从中心点开始, 裁剪指点大小圆形图片 */
+ (instancetype)circleImageWithName:(NSString *)name Size:(CGSize)size;
/** 裁剪图片为圆形, 并添加指定宽度,颜色的边框 */
+ (instancetype)circleImageWithName:(NSString *)name borderWidth:(CGFloat)borderWidth borderColor:(UIColor *)borderColor;

/** 返回targetSize大小的图片, 等比例拉伸, 居中 */
- (instancetype)thumbnailImageWithSize:(CGSize)targetSize;

@end
