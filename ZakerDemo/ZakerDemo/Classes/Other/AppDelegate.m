//
//  AppDelegate.m
//  ZakerDemo
//
//  Created by Linyanzuo on 11/27/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//

#import "AppDelegate.h"
#import "ZMainTabbarController.h"
#import "ZUserDefaultTool.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.window makeKeyAndVisible];
    
    [ZUserDefaultTool defaultThemeColor];
    
    ZMainTabbarController *tabbarVC = [[ZMainTabbarController alloc] init];
    [self.window setRootViewController:tabbarVC];

    return YES;
}


@end
