//
//  AppDelegate.h
//  ZakerDemo
//
//  Created by Linyanzuo on 11/27/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

