//
//  ZCommendViewController.m
//  ZakerDemo
//
//  Created by Linyanzuo on 12/11/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//

#import "ZCommendViewController.h"
#import "ZBarButtonItem.h"
#import "ZUserDefaultTool.h"

@implementation ZCommendViewController

- (instancetype)init {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:NSStringFromClass([self class]) bundle:nil];
    self = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];

    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupNavigationBar];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - 导航条相关

- (void)setupNavigationBar {
    self.navigationItem.rightBarButtonItem = [ZBarButtonItem itemWithImageName:@"DailyHot_PreferencesButton" Target:self Action:@selector(rightButtonAction)];
}

- (void)rightButtonAction {
    NSLog(@"%s", __func__);
}

@end















