//
//  ZSubscriptionViewController.m
//  ZakerDemo
//
//  Created by Linyanzuo on 12/11/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//
//  订阅界面 主控制器

#import "ZSubscriptionViewController.h"
#import "ZCollectionViewCell.h"
#import "ZPictureBrowserView.h"
#import "ZBarButtonItem.h"

@interface ZSubscriptionViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) NSArray *channelArr;

@end

@implementation ZSubscriptionViewController

static NSString *cellID = @"CellID";
static NSString *headerID = @"headerID";

- (instancetype)init {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:NSStringFromClass([self class]) bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavigationBar];
    
//    [self.collectionView registerClass:[ZCollectionViewCell class] forCellWithReuseIdentifier:cellID];
    [self.collectionView registerNib:[UINib nibWithNibName:@"ZCollectionViewCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:cellID];
    [self.collectionView registerClass:[ZPictureBrowserView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headerID];
}

#pragma mark - 导航条相关

- (void)setupNavigationBar {
    self.navigationItem.leftBarButtonItem = [[ZBarButtonItem alloc] initWithTitle:@"获取天气" style:UIBarButtonItemStylePlain target:self action:@selector(leftButtonAction)];
    self.navigationItem.rightBarButtonItem = [ZBarButtonItem itemWithImageName:@"addRootBlock_toolbar_add" Target:self Action:@selector(rightButtonAction)];
}

- (void)leftButtonAction {
    NSLog(@"%s", __func__);
}

- (void)rightButtonAction {
    NSLog(@"%s", __func__);
}

#pragma mark - UICollectionViewDelegate & UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 15;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ZCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellID forIndexPath:indexPath];
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    ZPictureBrowserView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headerID forIndexPath:indexPath];
    
    return headerView;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat width = self.view.width / 3;
    return CGSizeMake(width, width);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    
    CGFloat height = self.view.width / 16 * 9;
    return CGSizeMake(self.view.width, height);
}

@end




















