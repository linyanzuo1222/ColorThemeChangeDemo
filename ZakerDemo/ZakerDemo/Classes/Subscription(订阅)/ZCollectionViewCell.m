//
//  ZCollectionViewCell.m
//  ZakerDemo
//
//  Created by Linyanzuo on 12/12/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//

#import "ZCollectionViewCell.h"

@implementation ZCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initSubviews];
    }
    return self;
}

- (void)initSubviews {
    
}

@end
