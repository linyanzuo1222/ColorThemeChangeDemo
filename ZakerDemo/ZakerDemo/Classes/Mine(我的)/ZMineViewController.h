//
//  ZMineViewController.h
//  ZakerDemo
//
//  Created by Linyanzuo on 12/11/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZMineCellModel.h"
#import "ZMineCellArrowModel.h"
#import "ZMineCellSwitchModel.h"
#import "ZMineCellDetailModel.h"
#import "ZMineCellCustomModel.h"

@interface ZMineViewController : UITableViewController

@property (nonatomic, strong) NSArray *dataArr;
@property (nonatomic, copy) NSString *navigationTitle;

@end
