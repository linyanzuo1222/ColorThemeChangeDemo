//
//  ZMineCellArrowModel.h
//  ZakerDemo
//
//  Created by Linyanzuo on 12/12/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//

#import "ZMineCellModel.h"

@interface ZMineCellArrowModel : ZMineCellModel

@property (nonatomic,  assign) Class targetClass;

+ (instancetype)cellModelWithTitle:(NSString *)title TargetClass:(Class)targetClass;
+ (instancetype)cellModelWithTitle:(NSString *)title SubTitle:(NSString *)subTitle TargetClass:(Class)targetClass;

@end
