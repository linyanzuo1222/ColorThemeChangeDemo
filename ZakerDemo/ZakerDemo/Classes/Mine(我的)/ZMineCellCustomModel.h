//
//  ZMineCellCustomModel.h
//  ZakerDemo
//
//  Created by Linyanzuo on 12/14/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//

#import "ZMineCellModel.h"

@interface ZMineCellCustomModel : ZMineCellModel

@property (nonatomic, strong) UIView *customView;

+ (instancetype)cellModelWithTitle:(NSString *)title CustomView:(UIView *)customView;

@end
