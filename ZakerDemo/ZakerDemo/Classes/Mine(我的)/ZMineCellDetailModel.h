//
//  ZMineCellDetailModel.h
//  ZakerDemo
//
//  Created by Linyanzuo on 12/13/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//

#import "ZMineCellModel.h"

@interface ZMineCellDetailModel : ZMineCellModel

@property (nonatomic, strong) NSString *detailText;

+ (instancetype)cellModelWithTitle:(NSString *)title DetailText:(NSString *)detailText;

@end
