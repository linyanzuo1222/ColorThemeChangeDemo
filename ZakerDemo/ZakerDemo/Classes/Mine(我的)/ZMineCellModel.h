//
//  ZMineCellModel.h
//  ZakerDemo
//
//  Created by Linyanzuo on 12/12/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZMineCellModel : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subTitle;

- (instancetype)initWithTitle:(NSString *)title SubTitle:(NSString *)subTitle;
+ (instancetype)cellModelWithTitle:(NSString *)title;
+ (instancetype)cellModelWithTitle:(NSString *)title SubTitle:(NSString *)subTitle;

@end
