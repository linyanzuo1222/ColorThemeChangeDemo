//
//  ZMineRootViewController.m
//  ZakerDemo
//
//  Created by Linyanzuo on 12/13/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//

#import "ZMineRootViewController.h"
#import "UIImage+Extension.h"

#import "ZMoreViewController.h"
#import "ZAboutViewController.h"

@interface ZMineRootViewController ()

@end

@implementation ZMineRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.dataArr = @[
                 @[
                     [ZMineCellArrowModel cellModelWithTitle:@"好友分享" TargetClass:nil],
                     [ZMineCellArrowModel cellModelWithTitle:@"我的消息" TargetClass:nil],
                     [ZMineCellArrowModel cellModelWithTitle:@"推送资讯" TargetClass:nil],
                     [ZMineCellArrowModel cellModelWithTitle:@"积分活动" TargetClass:nil],
                     [ZMineCellArrowModel cellModelWithTitle:@"我的收藏" TargetClass:nil],
                     ],
                 @[
                     [ZMineCellArrowModel cellModelWithTitle:@"离线下载" TargetClass:nil],
                     [ZMineCellDetailModel cellModelWithTitle:@"清除缓存" DetailText:@"当前缓存22M"],
                     [ZMineCellSwitchModel cellModelWithTitle:@"夜间模式"],
                     [ZMineCellArrowModel cellModelWithTitle:@"更多设置" TargetClass:[ZMoreViewController class]],
                     ],
                 @[
                     [ZMineCellArrowModel cellModelWithTitle:@"意见反馈" TargetClass:nil],
                     [ZMineCellArrowModel cellModelWithTitle:@"关于Zaker v5.0.1" TargetClass:[ZAboutViewController class]],
                     ],
                 ];
    
    [self setupNavigationBar];
}

#pragma mark - 导航条相关

- (void)setupNavigationBar {
    self.navigationItem.title = @"";
    
    UIImage *image = [UIImage circleImageWithName:@"circle_default_avatar" Size:CGSizeMake(30, 30)];
    self.navigationItem.leftBarButtonItem = [ZBarButtonItem itemWithTitle:@"登录Zaker体验更多功能" Image:image Target:self Action:@selector(leftButtonAction)];
    
    self.navigationItem.rightBarButtonItem = [ZBarButtonItem itemWithImageName:@"addRootBlock_toolbar_next" Target:self Action:@selector(rightButtonAction)];
}

- (void)leftButtonAction {
    NSLog(@"%s", __func__);
}

- (void)rightButtonAction {
    NSLog(@"%s", __func__);
}

@end
