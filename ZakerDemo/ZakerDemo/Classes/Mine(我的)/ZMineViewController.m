//
//  ZMineViewController.m
//  ZakerDemo
//
//  Created by Linyanzuo on 12/11/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//

#import "ZMineViewController.h"
#include "ZMineCellView.h"

@interface ZMineViewController () 

@end

@implementation ZMineViewController

- (instancetype)init {
    if (self = [super init]) {
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = self.navigationTitle;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

#pragma mark - UITableViewDelegate && UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _dataArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_dataArr[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZMineCellView *cell = nil;
    ZMineCellModel *cellModel = _dataArr[indexPath.section][indexPath.row];
    
    if ([cellModel isKindOfClass:[ZMineCellDetailModel class]]) {
        cell = [ZMineCellView cellWithTableView:tableView Style:ZMineCellViewStyleDetail];
        ZMineCellDetailModel *detailModel = (ZMineCellDetailModel *)cellModel;
        
        cell.detailLabel.text = detailModel.detailText;
    }
    
    if ([cellModel isKindOfClass:[ZMineCellSwitchModel class]]) {
        cell = [ZMineCellView cellWithTableView:tableView Style:ZMineCellViewStyleSwitch];
    }
    
    if ([cellModel isKindOfClass:[ZMineCellArrowModel class]]) {
        cell =  [ZMineCellView cellWithTableView:tableView Style:ZMineCellViewStyleArrow];
    }
    
    if ([cellModel isKindOfClass:[ZMineCellCustomModel class]]) {
        ZMineCellCustomModel *customModel = (ZMineCellCustomModel *)cellModel;
        cell = [ZMineCellView cellWithTableView:tableView CustomView:customModel.customView];
    }
    
    cell.model = cellModel;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 0;
    }
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @" ";
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ZMineCellModel *cellModel = _dataArr[indexPath.section][indexPath.row];
    
    if ([cellModel isKindOfClass:[ZMineCellArrowModel class]]) {
        ZMineCellArrowModel *arrowModel = (ZMineCellArrowModel *)cellModel;
        
        if ( !arrowModel.targetClass) return;
        ZMineViewController *vc = [[arrowModel.targetClass alloc] init];
        
        if ( [vc isMemberOfClass:[ZMineViewController class]]) {
            vc.navigationTitle = arrowModel.title;
        }
        [self.navigationController pushViewController:vc animated:YES];
    }
}

@end














