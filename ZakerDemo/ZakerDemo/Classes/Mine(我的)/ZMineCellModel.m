//
//  ZMineCellModel.m
//  ZakerDemo
//
//  Created by Linyanzuo on 12/12/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//

#import "ZMineCellModel.h"

@implementation ZMineCellModel

- (instancetype)initWithTitle:(NSString *)title SubTitle:(NSString *)subTitle {
    if (self = [super init]) {
        self.title = title;
        self.subTitle = subTitle;
    }
    return self;
}

+ (instancetype)cellModelWithTitle:(NSString *)title {
    return [[self alloc] initWithTitle:title SubTitle:nil];
}

+ (instancetype)cellModelWithTitle:(NSString *)title SubTitle:(NSString *)subTitle {
    return [[self alloc] initWithTitle:title SubTitle:subTitle];
}

@end
