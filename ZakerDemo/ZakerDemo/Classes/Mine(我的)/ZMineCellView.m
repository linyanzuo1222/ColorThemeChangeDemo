//
//  ZMineCellView.m
//  ZakerDemo
//
//  Created by Linyanzuo on 12/12/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//

#import "ZMineCellView.h"
#import "ZMineCellModel.h"
#import "ZMineCellArrowModel.h"
#import "ZMineCellSwitchModel.h"
#import "ZUserDefaultTool.h"

#define ArrowViewRightMargin 10
#define SwitchViewRightMargin 20
#define DetailLabelRightMargin 20
#define CustomViewRightMargin 10
#define StyleIdentifier(style) [NSString stringWithFormat:@"ZMineCellViewID%zd", style]

@interface ZMineCellView () 

@property (nonatomic, strong) UIView *separatorLine;

@end

@implementation ZMineCellView

- (instancetype)initWithMineStyle:(ZMineCellViewStyle)style Identifier:(NSString *)identifier {
    /** 通过TableViewCell的initWithStyle: reuseIdentifier: 方法创建的Cell默认的size为320 * 44 */
    if (self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier]) {
        self.style = style;
        [self setupAppearance];
        [self initSubviews];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.style = ZMineCellViewStyleNone;
        [self setupAppearance];
        [self initSubviews];
    }
    return self;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView Style:(ZMineCellViewStyle)style {
    ZMineCellView *cell = [tableView dequeueReusableCellWithIdentifier:StyleIdentifier(style)];
    if ( !cell) {
        cell = [[ZMineCellView alloc] initWithMineStyle:style Identifier:StyleIdentifier(style)];
    }
    return cell;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView CustomView:(UIView *)customView {
    ZMineCellView *cell = [tableView dequeueReusableCellWithIdentifier:@"CustomView"];
    if ( !cell) {
        cell = [[ZMineCellView alloc] initWithMineStyle:ZMineCellViewStyleCustom Identifier:@"CustomView"];
        [cell initCustomView:customView];
    }
    return cell;
}

- (void)initCustomView:(UIView *)customView {
    if (customView) {
        _customView = customView;
        [self.contentView addSubview:_customView];
    }
}

/** 根据Style初始化子视图 */
- (void)initSubviews {
    if (_style == ZMineCellViewStyleArrow) {
        _arrowView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
        _arrowView.image = [UIImage imageNamed:@"addRootBlock_cell_next"];
        [self.contentView addSubview:_arrowView];
    }
    
    if (_style == ZMineCellViewStyleSwitch) {
        _switchView = [[UISwitch alloc] init];
        _switchView.onTintColor = [ZUserDefaultTool themeColor];
        [self.contentView addSubview:_switchView];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //The block is copied by the notification center and (the copy) held until the observer registration is removed.
        [[NSNotificationCenter defaultCenter] addObserverForName:ThemeChangeNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
            self.switchView.onTintColor = note.object;
        }];
    }
    
    if (_style == ZMineCellViewStyleDetail) {
        _detailLabel = [[UILabel alloc] init];
        _detailLabel.font = [UIFont systemFontOfSize:14];
        _detailLabel.textColor = [UIColor grayColor];
        [self.contentView addSubview:_detailLabel];
    }
    
    if (_style == ZMineCellViewStyleCustom) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    // 分割线
    _separatorLine = [[UIView alloc] init];
    [_separatorLine setBackgroundColor:[UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0]];
    [self.contentView addSubview:_separatorLine];
}

/** 
 TableView 通过CellForRowAtIndexPath: 获取Cell后, 会根据TableViewDelegate等实际参数去修改Cell的Frame值,
 之后通过layoutSubviews去调整subviews的布局以适应TableView的Cell显示.
 因此让自定义Cell适应TableView中的Cell高度变化时, 在LayoutSubviews中进行控件布局适配, 此时的Frame值才是正确的
 */
- (void)layoutSubviews {
    [super layoutSubviews];
    
    _separatorLine.frame = CGRectMake(0, self.height -  1, self.width, 1);
    
    if (_switchView) {
        _switchView.center = CGPointMake(self.width - _switchView.width / 2 - SwitchViewRightMargin, self.height / 2);
    }
    
    if (_arrowView) {
        _arrowView.center = CGPointMake(self.width - _arrowView.width / 2 - ArrowViewRightMargin,  self.height / 2);
    }
    
    if (_detailLabel) {
        [_detailLabel sizeToFit];
        _detailLabel.center = CGPointMake(self.width - _detailLabel.width / 2 - DetailLabelRightMargin, self.height / 2);
    }
    
    if (_customView) {
        _customView.center = CGPointMake(self.width - _customView.width / 2 - CustomViewRightMargin, self.height / 2);
    }
}

/** CellView样式设置 */
- (void)setupAppearance {
    self.textLabel.font = [UIFont boldSystemFontOfSize:15];
    self.detailTextLabel.font = [UIFont systemFontOfSize:12];
    self.detailTextLabel.textColor = [UIColor lightGrayColor];
}

/** 根据模型更新Cell内容 */
- (void)updateCell {
    self.textLabel.text = self.model.title;
    self.detailTextLabel.text = self.model.subTitle;
}

#pragma mark - Getter & Setter Method

// 在设置以cellModel后对Cell的内容进行更新
- (void)setModel:(ZMineCellModel *)model {
    _model = model;
    [self updateCell];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end














