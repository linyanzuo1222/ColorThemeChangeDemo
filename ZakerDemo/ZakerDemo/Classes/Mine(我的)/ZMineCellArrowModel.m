//
//  ZMineCellArrowModel.m
//  ZakerDemo
//
//  Created by Linyanzuo on 12/12/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//

#import "ZMineCellArrowModel.h"

@implementation ZMineCellArrowModel

+ (instancetype)cellModelWithTitle:(NSString *)title TargetClass:(Class)targetClass {
    ZMineCellArrowModel *model = [super cellModelWithTitle:title];
    model.targetClass = targetClass;
    
    return model;
}

+ (instancetype)cellModelWithTitle:(NSString *)title SubTitle:(NSString *)subTitle TargetClass:(Class)targetClass {
    ZMineCellArrowModel *model = [super cellModelWithTitle:title SubTitle:subTitle];
    model.targetClass = targetClass;
    
    return model;
}

@end
