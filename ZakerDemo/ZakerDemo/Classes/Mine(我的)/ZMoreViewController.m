//
//  ZAdvanceViewController.m
//  ZakerDemo
//
//  Created by Linyanzuo on 12/13/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//

#import "ZMoreViewController.h"
#import "ZNewsPushControllerViewController.h"
#import "ZUserDefaultTool.h"

#define SelectedColorIndex @"SelectedColorIndex"
#define SelectedDownloadIndex @"SelectedDownloadIndex"

@interface ZMoreViewController ()

@property (nonatomic, assign) NSInteger selectedColorIndex;
@property (nonatomic, assign) NSInteger selectedDownloadIndex;

@property (nonatomic, strong) UIView *colorView;
@property (nonatomic, strong) UISegmentedControl *segment;

@property (nonatomic, strong) NSArray *colorArr;
@property (nonatomic, strong) NSArray *themeColorArr;

@end

@implementation ZMoreViewController

- (instancetype)init {
    if (self = [super init]) {
        self.selectedColorIndex = [ZUserDefaultTool IndexWithID:SelectedColorIndex];
        self.selectedDownloadIndex = [ZUserDefaultTool IndexWithID:SelectedDownloadIndex];
        
        self.colorArr = @[
                          [UIColor colorWithRed:227/255.0 green:67/255.0 blue:103/255.0 alpha:1.0], //pink
                          [UIColor colorWithRed:48/255.0 green:165/255.0 blue:245/255.0 alpha:1.0], //blue
                          [UIColor colorWithRed:228/255.0 green:61/255.0 blue:52/255.0 alpha:1.0], //red
                          ];
        self.themeColorArr = @[ThemePinkBundle, ThemeBlueBundle, ThemeRedBundle];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"高级设置";
    
    self.dataArr = @[
                     @[
                         [ZMineCellArrowModel cellModelWithTitle:@"分享设置" SubTitle:@"第三方分享帐号绑定" TargetClass:nil],
                         ],
                     @[
                         [ZMineCellArrowModel cellModelWithTitle:@"要闻推送" SubTitle:@"实时推送重大新闻" TargetClass:[ZNewsPushControllerViewController class]],
                         [ZMineCellSwitchModel cellModelWithTitle:@"双击关闭文章"], 
                         ],
                     @[
                         [ZMineCellCustomModel cellModelWithTitle:@"主题选择" CustomView:self.colorView],
                         [ZMineCellSwitchModel cellModelWithTitle:@"彩色图标"],
                         [ZMineCellCustomModel cellModelWithTitle:@"加载图片" CustomView:self.segment],
                         ],
                     ];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

#pragma mark - SegmentController

- (UISegmentedControl *)segment {
    if ( !_segment) {
        _segment = [[UISegmentedControl alloc] initWithItems:@[@"所有网络", @"仅wifi", @"不下载"]];
        _segment.selectedSegmentIndex = self.selectedDownloadIndex;
        
        _segment.tintColor = [ZUserDefaultTool themeColor];
    }
    
    return _segment;
}

#pragma mark - 颜色按钮相关

- (UIView *)colorView {
    if ( !_colorView) {
        _colorView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 150, 30)];
        CGFloat rightMargin = 5;
        CGFloat viewMargin = 15;

        for (int i = 0; i < 3; i++) {
            UIButton *button = [self colorButtonWithColor:_colorArr[i]];
            button.tag = i;
            [button setFrame:CGRectMake(0, 0, 40, 30)];
            [button setCenter:CGPointMake(_colorView.width - button.width / 2 - i * (button.width + rightMargin) - viewMargin, _colorView.height / 2)];
            if (i == self.selectedColorIndex) {
                button.selected = YES;
            }
            [_colorView addSubview:button];
        }
    }
    
    return _colorView;
}

/** 颜色按钮创建 */
- (UIButton *)colorButtonWithColor:(UIColor *)color {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundColor:color];
    [button setImage:[UIImage imageNamed:@"style_lite_selected" ] forState:UIControlStateSelected];
    [button addTarget:self action:@selector(colorButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}

/** 颜色按钮事件 */
- (void)colorButtonAction:(UIButton *)button {
    [self.colorView.subviews enumerateObjectsUsingBlock:^(UIButton *obj, NSUInteger idx, BOOL *stop) {
        obj.selected = NO;
    }];
    button.selected = YES;
    [ZUserDefaultTool setIndex:button.tag WithID:SelectedColorIndex];
    
    [self changeTheme:button];
}

/** 主题选择 */
- (void)changeTheme:(UIButton *)button {
    if ([[ZUserDefaultTool themeColor] isEqual:button.backgroundColor]) return;
    
    [ZUserDefaultTool setThemeColor:button.backgroundColor];
    [ZUserDefaultTool setString:self.themeColorArr[button.tag] WithID:CurrentThemeBundle];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:ThemeChangeNotification object:button.backgroundColor];
    
    // 改变当面界面的主题颜色
    self.segment.tintColor = button.backgroundColor;
    [self.tableView reloadData];
}

@end
