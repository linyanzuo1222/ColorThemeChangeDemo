//
//  ZAboutViewController.m
//  ZakerDemo
//
//  Created by Linyanzuo on 12/13/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//

#import "ZAboutViewController.h"

@interface ZAboutViewController ()

@end

@implementation ZAboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dataArr = @[
                 @[
                     [ZMineCellArrowModel cellModelWithTitle:@"推荐给好友" TargetClass:nil],
                     [ZMineCellArrowModel cellModelWithTitle:@"软件介绍" TargetClass:nil],
                     [ZMineCellArrowModel cellModelWithTitle:@"给ZAKER打分" TargetClass:nil],
                     ],
                 ];
}


@end
