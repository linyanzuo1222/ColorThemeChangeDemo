//
//  ZMineCellSwitchModel.h
//  ZakerDemo
//
//  Created by Linyanzuo on 12/12/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//

#import "ZMineCellModel.h"

@interface ZMineCellSwitchModel : ZMineCellModel

@property (assign, nonatomic, getter=isOn) BOOL on;

@end
