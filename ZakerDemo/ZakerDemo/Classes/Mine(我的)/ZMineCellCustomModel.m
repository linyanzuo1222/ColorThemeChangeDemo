//
//  ZMineCellCustomModel.m
//  ZakerDemo
//
//  Created by Linyanzuo on 12/14/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//

#import "ZMineCellCustomModel.h"

@implementation ZMineCellCustomModel

+ (instancetype)cellModelWithTitle:(NSString *)title CustomView:(UIView *)customView {
    ZMineCellCustomModel *model = [super cellModelWithTitle:title];
    model.customView = customView;
    
    return model;
}

@end
