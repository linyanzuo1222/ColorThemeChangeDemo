//
//  ZMineCellView.h
//  ZakerDemo
//
//  Created by Linyanzuo on 12/12/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ZMineCellModel;

typedef enum {
    ZMineCellViewStyleArrow,
    ZMineCellViewStyleSwitch,
    ZMineCellViewStyleDetail,
    ZMineCellViewStyleCustom,
    ZMineCellViewStyleNone,
}ZMineCellViewStyle;

@interface ZMineCellView : UITableViewCell

@property (strong, nonatomic) UISwitch *switchView;
@property (strong, nonatomic) UIImageView *arrowView;
@property (strong, nonatomic) UILabel *detailLabel;
@property (strong, nonatomic) UIView *customView;

@property (nonatomic, strong) ZMineCellModel *model;
@property (nonatomic, assign) ZMineCellViewStyle style;

+ (instancetype)cellWithTableView:(UITableView *)tableView Style:(ZMineCellViewStyle)style;
+ (instancetype)cellWithTableView:(UITableView *)tableView CustomView:(UIView *)customView;

@end
