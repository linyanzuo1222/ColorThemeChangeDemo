//
//  ZMineCellDetailModel.m
//  ZakerDemo
//
//  Created by Linyanzuo on 12/13/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//

#import "ZMineCellDetailModel.h"

@implementation ZMineCellDetailModel

+ (instancetype)cellModelWithTitle:(NSString *)title DetailText:(NSString *)detailText {
    ZMineCellDetailModel *model = [super cellModelWithTitle:title];
    model.detailText = detailText;
    
    return model;
}

@end
