//
//  ZNewsPushControllerViewController.m
//  ZakerDemo
//
//  Created by Linyanzuo on 12/14/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//

#import "ZNewsPushControllerViewController.h"

@interface ZNewsPushControllerViewController ()

@end

@implementation ZNewsPushControllerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dataArr = @[
                     @[
                         [ZMineCellSwitchModel cellModelWithTitle:@"突发新闻"],
                         [ZMineCellSwitchModel cellModelWithTitle:@"本地新闻"],
                         [ZMineCellSwitchModel cellModelWithTitle:@"早晚报"],
                         [ZMineCellSwitchModel cellModelWithTitle:@"星八刻"],
                         [ZMineCellSwitchModel cellModelWithTitle:@"夜读"],
                         ],
                     ];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45;
}

@end
