//
//  ZBarButtonItem.m
//  ZakerDemo
//
//  Created by Linyanzuo on 12/11/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//

#import "ZBarButtonItem.h"
#import "UIView+Extension.h"

@implementation ZBarButtonItem

- (instancetype)init {
    if (self = [super init]) {
        [self setupStyle];
    }
    return self;
}

- (void)setupStyle {
    NSDictionary *paramDict = @{
                                NSForegroundColorAttributeName : [UIColor whiteColor],
                                NSFontAttributeName : [UIFont systemFontOfSize:13]
                                };
    [self setTitleTextAttributes:paramDict forState:UIControlStateNormal];
}

+ (instancetype)itemWithImageName:(NSString *)imageName {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *image = [UIImage imageNamed:imageName];
    [button setImage:image forState:UIControlStateNormal];
    [button sizeToFit];

    return [[self alloc] initWithCustomView:button];
}

+ (instancetype)itemWithTitle:(NSString *)title Image:(UIImage *)image Target:(id)target Action:(SEL)action {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:13]];
    [button setTitle:title forState:UIControlStateNormal];
    [button setImage:image forState:UIControlStateNormal];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [button sizeToFit];
    
    return [[self alloc] initWithCustomView:button];
}

+ (instancetype)itemWithImageName:(NSString *)imageName Target:(id)target Action:(SEL)action {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *image = [UIImage imageNamed:imageName];
    [button setImage:image forState:UIControlStateNormal];
    [button sizeToFit];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    return [[self alloc] initWithCustomView:button];
}


@end
