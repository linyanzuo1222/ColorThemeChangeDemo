//
//  ZMainTabbarController.m
//  ZakerDemo
//
//  Created by Linyanzuo on 12/11/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//

#import "ZMainTabbarController.h"
#import "ZNavigationController.h"
#import "ZSubscriptionViewController.h"
#import "ZCommendViewController.h"
#import "ZTopicTableViewController.h"
#import "ZMineRootViewController.h"
#import "ZUserDefaultTool.h"

@implementation ZMainTabbarController

- (instancetype)init {
    if (self = [super init]) {
        // The tint color to apply to the tab bar’s tab bar items.
        self.tabBar.tintColor = [ZUserDefaultTool themeColor];
        [[NSNotificationCenter defaultCenter] addObserverForName:ThemeChangeNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
            self.tabBar.tintColor = note.object;
        }];

        [self setupTabbarButton];
    }
    return self;
}

- (void)setupTabbarButton {
    ZSubscriptionViewController *subscriptionVC = [[ZSubscriptionViewController alloc] init];
    [self addTabbarButtonWithController:subscriptionVC Title:@"订阅" ImageName:@"DashboardTabBarItemSubscription"];
    
    ZCommendViewController *commendVC = [[ZCommendViewController alloc] init];
    [self addTabbarButtonWithController:commendVC Title:@"推荐" ImageName:@"DashboardTabBarItemDailyHot"];
    
    ZTopicTableViewController *topicVC = [[ZTopicTableViewController alloc] init];
    [self addTabbarButtonWithController:topicVC Title:@"话题" ImageName:@"DashboardTabBarItemDiscussion"];
    
    ZMineRootViewController *mineVC = [[ZMineRootViewController alloc] init];
    [self addTabbarButtonWithController:mineVC Title:@"我的" ImageName:@"DashboardTabBarItemProfile"];
}

- (void)addTabbarButtonWithController:(UIViewController *)controller Title:(NSString *)title ImageName:(NSString *)imageName {
    controller.title = title;
    UIImage *image = [UIImage imageNamed:imageName];
    controller.tabBarItem.image = image;
    
    ZNavigationController *navVC = [[ZNavigationController alloc] initWithRootViewController:controller];
    
    [self addChildViewController:navVC];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
