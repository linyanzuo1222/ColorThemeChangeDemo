//
//  ZNavigationController.m
//  ZakerDemo
//
//  Created by Linyanzuo on 12/11/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//

#import "ZNavigationController.h"
#import "UIImage+Extension.h"
#import "ZUserDefaultTool.h"

@implementation ZNavigationController

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController {
    if (self = [super initWithRootViewController:rootViewController]) {
        [self setup];
    }
    return self;
}

/** 设置导航条元素 */
- (void)setup {
    [ZUserDefaultTool defaultString:ThemeBlueBundle WithID:CurrentThemeBundle];

    [self updataNavigationBarBackground];
    [self.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:ThemeChangeNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self updataNavigationBarBackground];
    }];
}

/** 更新导航栏背景图片 */
- (void)updataNavigationBarBackground {
    NSString *path =  [[NSBundle mainBundle] pathForResource:[ZUserDefaultTool stringWithID:CurrentThemeBundle] ofType:@"bundle"];
    path = [path stringByAppendingPathComponent:@"toolbar_bg.png"];
    UIImage *image = [UIImage imageWithContentsOfFile:path];
    
    // 将图片拉伸为适合 导航条 + 状态栏 背景
    image = [image thumbnailImageWithSize:CGSizeMake(self.view.width, 64)];
    [self.navigationBar setBackgroundImage:image  forBarMetrics:UIBarMetricsDefault];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    [self.navigationBar setBarTintColor:[ZUserDefaultTool themeColor]];
}

/** push前统一操作 */
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if (self.childViewControllers.count) {
        // 修改LeftButton后, 需要将NavigationController的interactivePopGestureRecognizer的delegate设置为nil, 才能实现左滑返回手势
        self.interactivePopGestureRecognizer.delegate = nil;
        viewController.hidesBottomBarWhenPushed = YES;
        viewController.navigationItem.leftBarButtonItem = [ZBarButtonItem itemWithImageName:@"addRootBlock_toolbar_return" Target:self Action:@selector(leftButtonAction)];
    }
    
    [super pushViewController:viewController animated:animated];
}

- (void)leftButtonAction {
    [self popViewControllerAnimated:YES];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end







