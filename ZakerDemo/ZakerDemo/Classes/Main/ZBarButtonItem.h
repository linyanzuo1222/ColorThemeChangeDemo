//
//  ZBarButtonItem.h
//  ZakerDemo
//
//  Created by Linyanzuo on 12/11/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZBarButtonItem : UIBarButtonItem

+ (instancetype)itemWithImageName:(NSString *)imageName;
+ (instancetype)itemWithImageName:(NSString *)imageName Target:(id)target Action:(SEL)action;
+ (instancetype)itemWithTitle:(NSString *)title Image:(UIImage *)image Target:(id)target Action:(SEL)action;

@end
