//
//  ZTopicTableViewController.m
//  ZakerDemo
//
//  Created by Linyanzuo on 12/11/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//

#import "ZTopicTableViewController.h"
#import "ZBarButtonItem.h"

@implementation ZTopicTableViewController

- (instancetype)init {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:NSStringFromClass([self class]) bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupNavigationBar];
}

#pragma mark - 导航条相关

- (void)setupNavigationBar {
    self.navigationItem.leftBarButtonItem = [ZBarButtonItem itemWithImageName:@"addRootBlock_toolbar_mailer" Target:self Action:@selector(leftButtonAction)];
    self.navigationItem.rightBarButtonItem = [ZBarButtonItem itemWithImageName:@"addRootBlock_toolbar_add" Target:self Action:@selector(rightButtonAction)];
}

- (void)leftButtonAction {
    NSLog(@"%s", __func__);
}

- (void)rightButtonAction {
    NSLog(@"%s", __func__);
}

#pragma mark - UITableViewDelegate && UITableViewDataSource



@end








