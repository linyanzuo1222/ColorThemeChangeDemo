//
//  ZTopicCellModel.h
//  ZakerDemo
//
//  Created by Linyanzuo on 12/14/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//

#import "ZMineCellView.h"

@interface ZTopicCellModel : ZMineCellView

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subTitle;

@end
