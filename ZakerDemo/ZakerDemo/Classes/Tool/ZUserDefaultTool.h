//
//  ZUserDefaultTool.h
//  ZakerDemo
//
//  Created by Linyanzuo on 12/15/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ThemeColor @"ThemeColor"
#define ThemeChangeNotification @"ThemeChangeNotification"
#define CurrentThemeBundle @"ThemeBundleName"

/** 主题Bundle名称 */
#define ThemeBlueBundle @"theme_blue"
#define ThemePinkBundle @"theme_pink"
#define ThemeRedBundle @"theme_red"
#define ThemeDarkBundle @"theme_dark"

@interface ZUserDefaultTool : NSObject
@property (nonatomic,weak) UIView *teView;
+ (void)defaultThemeColor;
+ (void)setThemeColor:(UIColor *)color;
+ (UIColor *)themeColor;

/** 默认值, 不存在时才会进行设置 */
+ (void)defaultIndex:(NSInteger)index WithID:(NSString *)identifier;
+ (void)setIndex:(NSInteger)index WithID:(NSString *)identifier;
+ (NSInteger)IndexWithID:(NSString *)identifier;

/** 默认值, 不存在时才会进行设置 */
+ (void)defaultString:(NSString *)string WithID:(NSString *)identifier;
+ (void)setString:(NSString *)string WithID:(NSString *)identifier;
+ (NSString *)stringWithID:(NSString *)identifier;

@end
