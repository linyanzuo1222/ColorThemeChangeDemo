//
//  ZUserDefaultTool.m
//  ZakerDemo
//
//  Created by Linyanzuo on 12/15/14.
//  Copyright (c) 2014 Zerosoft. All rights reserved.
//

#import "ZUserDefaultTool.h"

@implementation ZUserDefaultTool

#pragma mark - ThemeColor

+ (void)setThemeColor:(UIColor *)color {
    CGFloat red, green, blue, alpha;
    [color getRed:&red green:&green blue:&blue alpha:&alpha];
    NSArray *rgbArr = @[[NSString stringWithFormat:@"%f", red], [NSString stringWithFormat:@"%f", green], [NSString stringWithFormat:@"%f", blue], [NSString stringWithFormat:@"%f", alpha]];
    [[NSUserDefaults standardUserDefaults] setObject:rgbArr forKey:ThemeColor];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (UIColor *)themeColor {
    NSArray *rgbArr = [[NSUserDefaults standardUserDefaults] arrayForKey:ThemeColor];
    return [UIColor colorWithRed:[rgbArr[0] floatValue] green:[rgbArr[1] floatValue] blue:[rgbArr[2] floatValue] alpha:[rgbArr[3] floatValue]];
}

+ (void)defaultThemeColor {
    NSArray *rgbArr = [[NSUserDefaults standardUserDefaults] arrayForKey:ThemeColor];
    if ( !rgbArr) {
        [self setThemeColor: [UIColor colorWithRed:48/255.0 green:165/255.0 blue:245/255.0 alpha:1.0]];
    }
}

#pragma mark - Index

+ (void)defaultIndex:(NSInteger)index WithID:(NSString *)identifier {
    if ( ![[NSUserDefaults standardUserDefaults] stringForKey:identifier]) {
        [self setIndex:index WithID:identifier];
    }
}

+ (void)setIndex:(NSInteger)index WithID:(NSString *)identifier {
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%zd", index] forKey:identifier];
}

+ (NSInteger)IndexWithID:(NSString *)identifier {
    return [[[NSUserDefaults standardUserDefaults] stringForKey:identifier] integerValue];
}

#pragma mark - NSString

+ (void)defaultString:(NSString *)string WithID:(NSString *)identifier {
    if ( ![[NSUserDefaults standardUserDefaults] stringForKey:identifier]) {
        [self setString:string WithID:identifier];
    }
}

+ (void)setString:(NSString *)string WithID:(NSString *)identifier {
    [[NSUserDefaults standardUserDefaults] setObject:string forKey:identifier];
}

+ (NSString *)stringWithID:(NSString *)identifier {
    return [[NSUserDefaults standardUserDefaults] stringForKey:identifier];
}

@end















